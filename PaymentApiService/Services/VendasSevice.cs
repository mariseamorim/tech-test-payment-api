using Microsoft.EntityFrameworkCore;
using PaymentApiDomain.Entities;
using PaymentApiDomain.Enums;
using PaymentApiInfrastructure.Contexto;

namespace PaymentApiServices
{
    public class VendasService : IVendasService
    {
        private readonly DataContext _dbContext;

        public VendasService(DataContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public async Task<bool> AtualizarVenda(Venda venda, EStatus statusNovo)
        {
            var validacao = ValidarTransicaoDeStatus(venda.StatusVenda, statusNovo);
            if (validacao == EStatus.StatusInvalido)
                return false;
            venda.StatusVenda = validacao;
            _dbContext.Vendas.Update(venda);
            await _dbContext.SaveChangesAsync();

            return true;

        }

        public async Task<Venda> BuscarVendaPorId(int id)
        {
            return await _dbContext.Vendas.Include(v => v.Vendedor)
            .Include(p => p.Produtos)
            .FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<List<Venda>> ListarVendas()
        {
            return await _dbContext.Vendas.Include(v => v.Vendedor).Include(p => p.Produtos).ToListAsync();
        }

        public async Task RegistrarVenda(Venda venda)
        {
            await _dbContext.Vendas.AddAsync(venda);
            await _dbContext.SaveChangesAsync();

        }
        public EStatus ValidarTransicaoDeStatus(EStatus statusAtual, EStatus statusNovo)
        {

            if (statusAtual == EStatus.AguardandoPagamento && statusNovo == EStatus.PagamentoAprovado)
                return statusAtual = statusNovo;
            if (statusAtual == EStatus.AguardandoPagamento && statusNovo == EStatus.Cancelada)
                return statusAtual = statusNovo;
            if (statusAtual == EStatus.PagamentoAprovado && statusNovo == EStatus.EnviadoTransportadora)
                return statusAtual = statusNovo;
            if (statusAtual == EStatus.PagamentoAprovado && statusNovo == EStatus.Cancelada)
                return statusAtual = statusNovo;
            if (statusAtual == EStatus.EnviadoTransportadora && statusNovo == EStatus.Entregue)
                return statusAtual = statusNovo;
            else
                return EStatus.StatusInvalido;






        }

    }
}