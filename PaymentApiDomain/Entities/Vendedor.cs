using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentApiDomain.Entities
{
    public class Vendedor
    {
        public Vendedor(string cpf, string nome, string email, string telefone)
        {
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }
        public int Id { get; set; }
        [Required]
        public string Cpf { get; private set; }
        [Required]
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string Telefone { get; private set; }

    }
}