using System.ComponentModel.DataAnnotations;

namespace PaymentApiDomain.Entities
{
    public class Produto
    {
        public Produto(string nome, decimal preco)
        {
            Nome = nome;
            Preco = preco;
        }

        public int Id { get; set; }
        [Required]
        public string Nome { get; private set; }
        [Required]
        public decimal Preco { get; private set; }
        
    }
}