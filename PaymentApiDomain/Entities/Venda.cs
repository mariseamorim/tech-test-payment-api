
using System.ComponentModel.DataAnnotations;
using PaymentApiDomain.Enums;

namespace PaymentApiDomain.Entities
{
    public class Venda
    {
        public Venda(Vendedor vendedor)
        {
            Vendedor = vendedor;
            DataVenda = DateTime.Now;
            StatusVenda = EStatus.AguardandoPagamento;
            Produtos = new List<Produto>();
        }
        public Venda(Vendedor vendedor, DateTime date, EStatus status, List<Produto> produtos)
        {
            Vendedor = vendedor;
            DataVenda = date;
            StatusVenda = status;
            Produtos = produtos;
        }

        public Venda()
        {

        }
        public int Id { get; set; }
        [Required]
        public Vendedor Vendedor { get; set; }
        [Required]
        public IList<Produto> Produtos { get; set; }
        [Required]
        public DateTime DataVenda { get; set; }
        public EStatus StatusVenda { get; set; }
      
    }
}