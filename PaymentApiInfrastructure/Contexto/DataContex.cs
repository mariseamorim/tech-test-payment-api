using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using PaymentApiDomain.Entities;

namespace PaymentApiInfrastructure.Contexto
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Venda>().Property(v => v.Id).ValueGeneratedOnAdd();
            builder.Entity<Venda>(e =>
            {
                e.HasKey(v => v.Id);

            });


            builder.Entity<Vendedor>().Property(a => a.Id).ValueGeneratedOnAdd();

            builder.Entity<Vendedor>(e =>
            {
                e.HasKey(a => a.Id);

            });
            builder.Entity<Produto>().Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Entity<Produto>(e =>
            {
                e.HasKey(p => p.Id);

            });

        }
    }
}