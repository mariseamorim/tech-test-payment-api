using Microsoft.EntityFrameworkCore;
using PaymentApiDomain.Entities;
using PaymentApiDomain.Enums;
using PaymentApiInfrastructure.Contexto;
using PaymentApiServices;

namespace PaymentApiTest;

[TestClass]
public class VendasServiceTest
{
    private DbContextOptions<DataContext> dbContextOptions;

    public VendasServiceTest()
    {
        var dbName = $"Database_{DateTime.Now.ToFileTimeUtc()}";
        dbContextOptions = new DbContextOptionsBuilder<DataContext>()
            .UseInMemoryDatabase(dbName)
            .EnableSensitiveDataLogging(true)
            .Options;

    }
    [TestMethod]
    public async Task GetVendasAsync_Success_Test()
    {
        var repository = await CreateRepositoryAsync();

        // Act
        var vendasList = await repository.ListarVendas();

        // Assert
        Assert.AreEqual(2, vendasList.Count);
    }

    [TestMethod]
    public async Task GetVendasByIdAsync_Success_Test()
    {
        var repository = await CreateRepositoryAsync();

        // Act
        var venda = await repository.BuscarVendaPorId(1);

        // Assert
        Assert.IsNotNull(venda);
        Assert.AreEqual(EStatus.AguardandoPagamento, venda.StatusVenda);
        Assert.AreEqual(1, venda.Produtos.Count);
    }

    [TestMethod]
    public async Task CreateAsync_Success_Test()
    {
        var repository = await CreateRepositoryAsync();

        var vendedor = new Vendedor("451.887.033-60", "Emanuelly Isabel Brito", "emanuelly_brito@test.com.br", "(96) 2504-3436");
        // Act
        await repository.RegistrarVenda(new Venda()
        {
            Vendedor = vendedor,
            DataVenda = DateTime.Now,
            StatusVenda = EStatus.AguardandoPagamento,
            Produtos = new List<Produto>()
            {
                new Produto("Produto 1", 200)
            }
        });

        // Assert
        var vendasList = await repository.ListarVendas();
        Assert.AreEqual(3, vendasList.Count);
    }

    [TestMethod]
    public async Task UpdateStatusVendsync_Success_Test()
    {
        var repository = await CreateRepositoryAsync();

        // Act
        var venda = await repository.BuscarVendaPorId(1);
        await repository.AtualizarVenda(venda, EStatus.PagamentoAprovado);

        // Assert
        Assert.IsNotNull(venda);
        Assert.AreEqual(EStatus.PagamentoAprovado, venda.StatusVenda);
        Assert.AreEqual(1, venda.Produtos.Count);
    }

    private async Task<VendasService> CreateRepositoryAsync()
    {
        DataContext context = new DataContext(dbContextOptions);
        await PopulateDataAsync(context);
        return new VendasService(context);
    }

    private async Task PopulateDataAsync(DataContext context)
    {
        int index = 1;
        while (index <= 2)
        {
            var venda = new Venda()
            {
                Vendedor = new Vendedor("451.887.033-60", "Emanuelly Isabel Brito", "emanuelly_brito@test.com.br", "(96) 2504-3436"),
                DataVenda = DateTime.Now,
                StatusVenda = EStatus.AguardandoPagamento,
                Produtos = new List<Produto>()
                {
                    new Produto("Caneca", 15),
                }
            };

            index++;
            await context.Vendas.AddAsync(venda);
        }

        await context.SaveChangesAsync();
    }
}