using PaymentApiDomain.Entities;
using PaymentApiDomain.Enums;

namespace PaymentApiServices
{
    public interface IVendasService
    {
        Task RegistrarVenda(Venda venda);
        Task<Venda> BuscarVendaPorId(int id);
        Task<bool> AtualizarVenda(Venda venda, EStatus statusNovo);
        Task<List<Venda>> ListarVendas();

    }

}