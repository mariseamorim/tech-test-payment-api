using System.Net;
using Microsoft.AspNetCore.Mvc;
using PaymentApiDomain.Entities;
using PaymentApiDomain.Enums;
using PaymentApiServices;

namespace PaymentWebApiControllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly IVendasService _service;

        public VendasController(IVendasService service)
        {
            _service = service;
        }


        [HttpPost]
        public ActionResult<Venda> RegistrarVenda([FromBody] Venda venda)
        {
            try
            {
                if (venda.StatusVenda != EStatus.AguardandoPagamento)
                    return BadRequest($"O status de criação deve ser {EStatus.AguardandoPagamento.GetDescription()}");
                if (venda.Produtos.Count > 0)
                {
                    _service.RegistrarVenda(venda);
                    return CreatedAtAction(nameof(BuscarVendaPorId), new { id = venda.Id }, venda);
                }
                return BadRequest("Informe ao menos um produto");

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("{id}")]
        public ActionResult<Venda> BuscarVendaPorId([FromRoute] int id)
        {
            var result = _service.BuscarVendaPorId(id);
            if (result is null) return NoContent();
            return Ok(result);
        }
        [HttpGet]
        public ActionResult<List<Venda>> ListarVendas()
        {
            var result = _service.ListarVendas();
            if (result == null)
                return NoContent();
            return Ok(result);
        }


        [HttpPut("{id}")]
        public ActionResult<Object> AtualizarVenda([FromRoute] int id, EStatus statusNovo)
        {
            var venda = _service.BuscarVendaPorId(id);

            if (venda.Result is null) return BadRequest(new
            {
                msg = "Registro não encontrado",
                status = HttpStatusCode.NotFound
            });
            try
            {
                var vendaAtualizada = _service.AtualizarVenda(venda.Result, statusNovo);
                if (vendaAtualizada.Result)
                    return Ok("Venda atualizada");
                else
                {
                    return BadRequest(new
                    {

                        msg = $"Ocorreu um erro de atualização do status atual {venda.Result.StatusVenda.GetDescription()} para o status {statusNovo.GetDescription()} no id {id}",
                        status = HttpStatusCode.BadRequest
                    });
                }
            }
            catch (Exception)
            {
                return BadRequest(new
                {
                    msg = $"Ocorreu um erro de atualização para o id {id}",
                    status = HttpStatusCode.BadRequest
                });
            }
        }


    }
}